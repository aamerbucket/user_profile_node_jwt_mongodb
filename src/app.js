const express = require('express');
const register_user = require('./routers/register_user');
const login_user = require('./routers/login_user');
const port = process.env.PORT;
require('./db/db');
const app = express();

app.use(express.json());
app.use('/users', register_user);
app.use('/users', login_user);

app.listen(port, ()=>{
    console.log(`server running on port ${port}`);
});