const jwt = require('jsonwebtoken')
const User = require('../models/User')
// To verify the token with system variable
const auth = async (req, res, next)=>{
        const token = req.header('x-auth-token');
        // check whether the token is available or not
        if(!token) {
            return res.status(401).send('Access denied. No token provided.');
        } 
        try {
            const decodedPayload = jwt.verify(token, process.env.JWT_KEY)
            req.user = decodedPayload
            next() // needs to be called, else the application will be frozen.
        } catch (ex) {
            res.status(400).send('Invalid Token')
        }
    
    }

module.exports = auth;